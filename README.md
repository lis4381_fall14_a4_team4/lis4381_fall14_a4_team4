# README #

This README is for LIS4381 and the group 4 team.  We are working on learning Bitbucket and learning how to use it to collaborate on a shared project.

### What is this repository for? ###

This repository contains a bootstrap template that we are using to implement changes together.  It also contains other miscellaneous files.

### How do I get set up? ###

Dr. Jowett has a ton of links with tutorials.  I would recommend starting there or using Google. 

### Contribution guidelines ###

The team just needs to make one change to the project file to show they know how to use Bitbucket and the git commands.

### Who do I talk to? ###

Dr. Jowett is always looking to chat it up with someone or you can get a hold of one of your team members. 